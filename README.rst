pdffigures2python
====

A Python ease-of-access wrapper for the Scala pdffigures2 
package (https://github.com/allenai/pdffigures2).

Setup
-----

1. Install scala and sbt on your system.

2. Install pdffigures:

`pip install git+https://gitlab.com/aperz/pdffigures`

3. In Python:

>>> import pdffigures
>>> pdffigures.initialize_scala_project() 


Use
---

>>> import pdffigures
>>> pdffile = pdffigures.PDFFile("path/to/file.pdf")

Change installation directory
-----------------------------

By default, pdffigures2python will build pdffigures2 in `~/.pdffigures/pdffigures2`.
To change this, edit the `pdffigures/config.py` file.
