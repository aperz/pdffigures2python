#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import json
import os
import subprocess as sp
import inspect
import time
from shutil import copyfile

from pdffigures.config import cfg

# TODO: fix the location
SCALA_PROJECT_LOCATION = cfg["scala_project_location"]


def initialize_scala_project(scala_location=SCALA_PROJECT_LOCATION, force=False):
    #loc = os.path.split(inspect.getfile(PDFFile))[0]
    package_location = os.path.dirname(os.path.realpath(__file__))
    scala_location = os.path.join(scala_location, "pdffigures2")

    if not os.path.exists(os.path.join(scala_location, "build.sbt")) or force:
        print("pdffigures: Initializing the Scala project")
        print("pdffigures: (Nonzero exit code error is OK, running the command with no arguments causes that)")
        os.makedirs(scala_location, exist_ok=True)
        #sp.call("cp " + os.path.join(package_location, "build.sbt") + " " + os.path.join(scala_location, "build.sbt"), shell=True)
        copyfile(os.path.join(package_location, "build.sbt"),
                    os.path.join(scala_location, "build.sbt"))

        #FIXME: run Scala from current dir instead of switching to its location
        #TODO: Also, move the default location to ~/.pdffigures
        ecode = sp.call('sbt "runMain org.allenai.pdffigures2.FigureExtractorBatchCli"',
                cwd = scala_location,
                shell=True)
    else:
        print("pdffigures: Not re-initializing the Scala project (pdffigures2 folder found). Use force=True to overwrite.")

def clean_old_files(dest):
    if os.path.exists(dest):
        for f in os.listdir(dest):
            f = os.path.join(dest, f)
            if os.path.splitext(f)[-1] == ".json":
                os.remove(f)

class ScalaError(Exception):
    def __init__(self, message):
        self.message = message

class PDFFile(object):
    def __init__(self, filepath, scala_location):#=SCALA_PROJECT_LOCATION):
        assert isinstance(filepath, str)
        scala_location = os.path.join(scala_location, "pdffigures2")

        self.filepath = os.path.join(os.getcwd(), filepath)

        clean_old_files(scala_location)

        #self.json_filepath = os.path.splitext("text_"+os.path.split(self.filepath)[1])[0]+".json"
        self.json_filepath = os.path.join(
                                scala_location,
                                os.path.splitext("text_"+os.path.split(self.filepath)[1])[0]+".json"
                                )

        #if not os.path.exists(self.json_filepath):

        call_extract_text = 'sbt "runMain org.allenai.pdffigures2.FigureExtractorBatchCli '+self.filepath+' -g ' + 'text_"'
        #call_extract_figures = 'sbt "runMain org.allenai.pdffigures2.FigureExtractorBatchCli '+fpath+' -d image_ -m data_"'

        try:
            sp.check_call(call_extract_text, cwd = scala_location, shell=True)
        except Exception as e:
            return e

        if not os.path.exists(self.json_filepath):
            raise ScalaError( "There were errors in the execution of the Scala code. Is the project definition accessible from the execution directory? Does the PDF file exist?")

        with open(self.json_filepath, "r") as handle:
            self.full_text = json.load(handle)


    def sections(self):
        for section in self.full_text['sections']:
            s = ""
            if 'title' in section:
                s += section['title']['text'] + "\n\n"
            for  paragraph in section['paragraphs']:
                s += paragraph['text'] + "\n"
            yield s


    def tables(self):
        for figure in self.full_text['figures']:
            if figure['figType'] == "Table":
                yield (figure['caption'], " ".join(figure['imageText']))
                #yield "TEXT    "+ figure['imageText'][0][:40] #is a list - representation of a table
                #[:40] #is a list - representation of a table

    def figures(self):
        pass

