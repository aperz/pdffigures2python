from setuptools import setup, find_packages
import os


DEPENDENCIES = [
    "pyyaml"
]

setup(
    name='pdfx',
    version='0.0.2',
    description='Extraction of features from PDF files.',
    url='http://gitlab.com/aperz/pdfx',
    author='Aleksandra Perz',
    author_email='perz.aleksandra@gmail.com',
    license='',
    packages=find_packages(),
    #py_modules=["pdfx"],
    data_files=[('pdfx', ['pdfx/build.sbt'])],
    #package_data={"pdfx":['config.yml']},
    include_package_data=True,
    install_requires=DEPENDENCIES,
    zip_safe=False,
    )

